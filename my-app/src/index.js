import React, {Component} from 'react';
import InjectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import {teal500} from 'material-ui/styles/colors';
import {indigo900} from 'material-ui/styles/colors';
import {Card, CardActions, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import FontIcon from 'material-ui/FontIcon';
import Avatar from 'material-ui/Avatar';
import ReactDOM from 'react-dom';
import './App.css';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: teal500,

  }
});
const iconStyles = {
  marginLeft: 50,
};

class App extends Component {
    render() {
        return(
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    <AppBar title="My Resume" />
                    <br/>
                    <div className="container">
                        <Avatar src="images/uxceo-128.jpg" size={150}/>
                        <br/><br/>
                        <h1>DEEPIKA AGGRAWAL</h1>
                        <p1>daggrawa@purdue.edu</p1>
                        <FontIcon className="muidocs-icon-action-home" style={iconStyles} color={teal500} />
                    </div>
                    <br/><br/>
                    <Card>
                        <CardHeader title="EDUCATION" titleColor={indigo900}/>
                    </Card>
                    <br/>
                    <Card>
                        <CardHeader title="EXPERIENCE" titleColor={indigo900}/>
                        <CardText>
                             Lorem ipsum dolor sit amet, consectetur adipiscing elit.Donec mattis pretium massa. <br/>Aliquam erat volutpat. <br/>Nulla facilisi.Donec vulputate interdum                                 sollicitudin.lacinia auctor quam sed pellentesque.Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                        </CardText>
                    </Card>

                </div>
            </MuiThemeProvider>

        );

    };
};

ReactDOM.render(<App />, document.getElementById('root'));