/**
 * In this file, we create a React component
 * which incorporates components provided by Material-UI.
 */
import React, {Component} from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import AppBar from 'material-ui/AppBar';

const Main = () => (


      <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
          //<div className="container">
            <AppBar title="Material-UI"/>
          //</div>
      </MuiThemeProvider>

);

export default Main;

